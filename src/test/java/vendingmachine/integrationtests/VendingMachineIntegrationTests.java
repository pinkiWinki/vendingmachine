package vendingmachine.integrationtests;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import vendingmachine.VendingMachine;
import vendingmachine.change.BasicMoneyChanger;
import vendingmachine.change.Change;
import vendingmachine.change.IMoneyChanger;
import vendingmachine.change.IMoneyChanger.CoinType;
import vendingmachine.change.RealisticMoneyChanger;
import vendingmachine.change.solvers.IChangeSolver;
import vendingmachine.change.solvers.RecursiveChangeSolver;



public class VendingMachineIntegrationTests{

	
	@Test
	public void VendingChangeWithBasicMoneyChangeBehaviour(){
		IMoneyChanger changer = new BasicMoneyChanger();
		
		VendingMachine vendingMachine = new VendingMachine(changer);
		
		vendingMachine.add(CoinType.TWO_EURO);
		
		String actual = vendingMachine.getDrink("Coke");
		String expected = "Your Coke and your change:zero";
		
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void VendingChangeWithRealisticMoneyChangeBehaviour(){
		
		HashMap<CoinType, Integer> initialChange = new HashMap<CoinType, Integer>();
		initialChange.put(CoinType.FIFTY_CTS, 5);
		initialChange.put(CoinType.TEN_CTS, 2);
		initialChange.put(CoinType.TWENTY_CTS,4);
		initialChange.put(CoinType.ONE_EURO,1);
		
		IChangeSolver solver = new RecursiveChangeSolver();		
		IMoneyChanger changer = new RealisticMoneyChanger(initialChange, solver);
		
		VendingMachine vendingMachine = new VendingMachine(changer);
		vendingMachine.add(CoinType.TWO_EURO);
		
		String actual = vendingMachine.getDrink("Coke");
		String expected = "Your Coke and your change:{FIFTY_CTS=1, TEN_CTS=1, TWENTY_CTS=1}";
		
		assertEquals(expected.length(), actual.length());
	}
}
	
