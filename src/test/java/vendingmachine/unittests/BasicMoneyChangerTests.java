package vendingmachine.unittests;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import vendingmachine.change.BasicMoneyChanger;
import vendingmachine.change.IMoneyChanger.CoinType;

public class BasicMoneyChangerTests{
	BasicMoneyChanger sut;

	
	@Before
	public void setUp()
	{
		
		sut = new BasicMoneyChanger();
	}

	@Test
	public void shouldAddTenCents() {
		sut.add(CoinType.TEN_CTS);
		int expected = 10;
		int actual = sut.getMoney();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void shouldAddFive() {
		sut.add(CoinType.FIVE_CTS);
		int expected = 5;
		int actual = sut.getMoney();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void shouldAddFiveCtsAnd1Euro() {
		sut.add(CoinType.FIVE_CTS);
		sut.add(CoinType.ONE_EURO);
		
		int expected = 105;
		int actual = sut.getMoney();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void shouldNotReturnAnyChange() {
		sut.add(CoinType.FIVE_CTS);
		sut.add(CoinType.ONE_EURO);

		assertTrue(sut.returnChange(50).isEnoughChange());
		
		int expected = 0;
		int actual = sut.getMoney();
		assertEquals(expected, actual);
	}

	
	
}
	
