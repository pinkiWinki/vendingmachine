package vendingmachine.unittests;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import vendingmachine.change.Change;
import vendingmachine.change.IMoneyChanger;
import vendingmachine.change.RealisticMoneyChanger;
import vendingmachine.change.IMoneyChanger.CoinType;
import vendingmachine.change.solvers.IChangeSolver;

public class RealisticMoneyChangerTests{
	RealisticMoneyChanger sut;
	IChangeSolver calc;
	Map<CoinType, Integer> change;

	
	@Before
	public void setUp()
	{
		change = new HashMap<CoinType, Integer>();
		change.put(CoinType.FIFTY_CTS, 5);
		change.put(CoinType.TEN_CTS, 2);
		change.put(CoinType.TWENTY_CTS,4);
		change.put(CoinType.ONE_EURO,1);
		
		calc = mock(IChangeSolver.class);
		
		sut = new RealisticMoneyChanger(change,calc);
	}

	@Test
	public void shouldAddTenCents() {
		sut.add(CoinType.TEN_CTS);
		int expected = 10;
		int actual = sut.getMoney();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void shouldAddFive() {
		sut.add(CoinType.FIVE_CTS);
		int expected = 5;
		int actual = sut.getMoney();
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void shouldAddFiveCtsAnd1Euro() {
		sut.add(CoinType.FIVE_CTS);
		sut.add(CoinType.ONE_EURO);
		
		int expected = 105;
		int actual = sut.getMoney();
		
		assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnTheExactChangeAndIs60cts() {
		
		Map<CoinType,Integer> changeToReturn = new HashMap<IMoneyChanger.CoinType, Integer>();
		
		changeToReturn.put(CoinType.TEN_CTS, 1);
		changeToReturn.put(CoinType.FIFTY_CTS, 1);
		
		when(calc.searchChange(60, change)).thenReturn(changeToReturn);
		

		Change actual = sut.returnChange(60);
		assertTrue(actual.isEnoughChange());
	
		int expectedMoney = 0;
		int actualMoney = sut.getMoney();
		assertEquals(expectedMoney, actualMoney);
	}
	
	@Test
	public void shouldReturnTheMoneyIfThereIsNoEnoughtChange() {
		sut.add(CoinType.FIFTY_CTS);
		sut.add(CoinType.FIFTY_CTS);
		
		Map<CoinType,Integer> changeToReturn = new HashMap<IMoneyChanger.CoinType, Integer>();
		changeToReturn.put(CoinType.ONE_EURO, 1);
		
		when(calc.searchChange(5, change)).thenReturn(new HashMap<>());
		when(calc.searchChange(100, change)).thenReturn(changeToReturn);
		
		Change actual = sut.returnChange(5);
		assertFalse(actual.isEnoughChange());
		
		int expectedMoney = 0;
		int actualMoney = sut.getMoney();
		assertEquals(expectedMoney, actualMoney);
	}
	
	
}
	
