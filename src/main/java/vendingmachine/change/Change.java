package vendingmachine.change;

import java.util.Map;

import vendingmachine.change.IMoneyChanger.CoinType;

public class Change {
	private Map<CoinType,Integer> change;
	private boolean enoughChange;
	
	public Map<CoinType, Integer> getChange() {
		return change;
	}
	public void setChange(Map<CoinType, Integer> change) {
		this.change = change;
	}
	public boolean isEnoughChange() {
		return enoughChange;
	}
	public void setEnoughChange(boolean canChange) {
		this.enoughChange = canChange;
	}

}
