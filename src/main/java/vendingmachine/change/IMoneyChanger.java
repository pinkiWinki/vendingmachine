package vendingmachine.change;

public interface IMoneyChanger {
	
	public enum CoinType{
		FIVE_CTS(5),
		TEN_CTS(10),
		TWENTY_CTS(20),
		FIFTY_CTS(50),
		ONE_EURO(100),
		TWO_EURO(200);
		private final int value;
		CoinType(int v){value=v;}
		public int getValue() { return value; }
		
	}

	/*!
	 * Add a coin into the money changer
	 */
	void add(CoinType coin);

	/*!
	 * Get the accumulated money in cents
	 */
	int getMoney();

	/*!
	 * Return change 
	 */
	Change returnChange(int productCost);

}