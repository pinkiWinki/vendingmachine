package vendingmachine.change;

import vendingmachine.change.IMoneyChanger.CoinType;

public class BasicMoneyChanger implements IMoneyChanger {
	private int money;


	public BasicMoneyChanger() {
		money = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vendingmachine.IMoneyChanger#add(java.lang.String)
	 */
	@Override
	public void add(CoinType coin) {

		money = money + coin.getValue();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vendingmachine.IMoneyChanger#getMoney()
	 */
	@Override
	public int getMoney() {

		return money;
	}

	@Override
	public Change returnChange(int cost) {
		money = 0;
		Change change = new Change();
		change.setEnoughChange(true);
		return change;
	}
}
