package vendingmachine.change.solvers;

import java.util.Map;

import vendingmachine.change.IMoneyChanger;
import vendingmachine.change.IMoneyChanger.CoinType;

public interface IChangeSolver {

	Map<CoinType, Integer> searchChange(int moneyToReturn, Map<CoinType, Integer> change);

}