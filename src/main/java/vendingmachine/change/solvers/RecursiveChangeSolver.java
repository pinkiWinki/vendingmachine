package vendingmachine.change.solvers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vendingmachine.change.IMoneyChanger;
import vendingmachine.change.IMoneyChanger.CoinType;

public class RecursiveChangeSolver implements IChangeSolver {
	
	/* (non-Javadoc)
	 * @see vendingmachine.IChangeCalculator#searchChange(int, java.util.Map)
	 */
	@Override
	public Map<CoinType, Integer> searchChange(int moneyToReturn, Map<CoinType, Integer> change) {
		Map<CoinType, Integer> changeBag = new HashMap<>();
		Map<CoinType, Integer> changeaux = new HashMap<>(change);
		List<CoinType> l = new ArrayList<>(Arrays.asList(CoinType.values()));
		Collections.sort(l);
		Collections.reverse(l);
		return searchChange(moneyToReturn, changeaux, changeBag, l);
	}

	private Map<CoinType, Integer> searchChange(int moneyToReturn, Map<CoinType, Integer> change,
			Map<CoinType, Integer> changeBag, List<CoinType> types) {
		if (moneyToReturn == 0)
			return changeBag;
		else {

			for (CoinType type : types) {

				int newProductCost = moneyToReturn - type.getValue();
				if (change.containsKey(type) && change.get(type) > 0 && newProductCost >= 0) {

					int c = change.get(type) - 1;
					change.put(type, c);
					changeBag.put(type, changeBag.get(type) != null ? changeBag.get(type) + 1 : 1);
					return searchChange(newProductCost, change, changeBag, types);

				} else {
					List<CoinType> l = new ArrayList<>(types);
					l.remove(type);
					return searchChange(moneyToReturn, change, changeBag, l);
				}
			}
			return new HashMap<>();
		}
	}

}
